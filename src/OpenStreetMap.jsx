import React, { Component } from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import L from "leaflet";
import axios from "axios";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});

class OpenStreetMap extends Component {
  constructor() {
    super();
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
      address: null
    };
  }

  handleMarkerDrag = e => {
    const _latlng = e.target._latlng;
    console.log(_latlng);
    this.setState({ ..._latlng });
    this.getFormattedAddress(_latlng);
  };

  getFormattedAddress = async locObj => {
    try {
      const geoCodeResp = await axios(
        `https://nominatim.openstreetmap.org/reverse?format=json&lat=${
          locObj.lat
        }&lon=${locObj.lng}&zoom=18&addressdetails=1`
      );
      if (geoCodeResp.status === 200) {
        const address = geoCodeResp.data;
        this.setState({
          address: address
        });
      }
    } catch (e) {}
  };

  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <div id="container">
        <Map
          center={position}
          zoom={this.state.zoom}
          onLocationfound={this.handleLocationFound}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
          <Marker
            position={position}
            draggable
            onDragend={this.handleMarkerDrag}
          >
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </Map>
        <pre>{JSON.stringify(this.state.address, null, 4)}</pre>
      </div>
    );
  }
}

export default OpenStreetMap;
